#pragma once
#include "icg_helper.h"
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/matrix_transform.hpp>

/*
 * Simple class to create and manage the grid
 * No texture, but manages uniform MVP as three matrices, namely, M, V, P in the shader
 */
class SimpleGrid {

protected:
    GLuint vertex_array_id_;                // vertex array object
    GLuint vertex_buffer_object_position_;  // memory buffer for positions
    GLuint vertex_buffer_object_index_;     // memory buffer for indices
    GLuint program_id_;                     // GLSL shader program ID

    GLuint M_id_;                         // model matrix ID
    GLuint V_id_;
    GLuint P_id_;

    unsigned long num_indices_;
    float size_;
    int resolution_;

public:
    void Init(std::string shader_base_name, float size = 16.0f, int resolution = 512) {
        program_id_ = icg_helper::LoadShaders((shader_base_name + std::string("_vshader.glsl")).c_str(),
                                              (shader_base_name + std::string("_fshader.glsl")).c_str());
        if(!program_id_) {
            exit(EXIT_FAILURE);
        }

        size_ = size;
        resolution_ = resolution;

        glUseProgram(program_id_);

        // vertex one vertex array
        glGenVertexArrays(1, &vertex_array_id_);
        glBindVertexArray(vertex_array_id_);
        // vertex coordinates and indices
        {
            std::vector<GLfloat> vertices;
            std::vector<GLuint> indices;

            GLuint gridDim_id = glGetUniformLocation(program_id_, "gridDim");
            glUniform1f(gridDim_id, size_);

            // vertex position  the triangles.
            for(float y = -size/2; y <= size/2; y += size/resolution_) {
                for(float x = -size/2; x <= size/2; x += size/resolution_) {
                    vertices.push_back(x); vertices.push_back(y);
                }
            }

            resolution_++;
            for (int y = 0; y < resolution_ - 1; y++) {
                for (int x = 0; x < resolution_; x++) {
                    indices.push_back((y * resolution_) + x);
                    indices.push_back(((y + 1) * resolution_) + x);
                }
                indices.push_back(65536);
            }
            resolution_--;

            num_indices_ = indices.size();

            // position buffer
            glGenBuffers(1, &vertex_buffer_object_position_);
            glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_object_position_);
            glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(GLfloat),
                         &vertices[0], GL_STATIC_DRAW);

            // vertex indices
            glGenBuffers(1, &vertex_buffer_object_index_);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vertex_buffer_object_index_);
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint),
                         &indices[0], GL_STATIC_DRAW);

            // position shader attribute
            GLuint loc_position = glGetAttribLocation(program_id_, "position");
            glEnableVertexAttribArray(loc_position);
            glVertexAttribPointer(loc_position, 2, GL_FLOAT, DONT_NORMALIZE,
                                  ZERO_STRIDE, ZERO_BUFFER_OFFSET);
        }

        // other uniforms
        V_id_ = glGetUniformLocation(program_id_, "V");
        M_id_ = glGetUniformLocation(program_id_, "M");
        P_id_ = glGetUniformLocation(program_id_, "P");

        // to avoid the current object being polluted
        glBindVertexArray(0);
        glUseProgram(0);
    }

    virtual void Cleanup() {
        glBindVertexArray(0);
        glUseProgram(0);
        glDeleteBuffers(1, &vertex_buffer_object_position_);
        glDeleteBuffers(1, &vertex_buffer_object_index_);
        glDeleteVertexArrays(1, &vertex_array_id_);
        glDeleteProgram(program_id_);
    }

    virtual void Draw(const glm::mat4 &model = IDENTITY_MATRIX,
                      const glm::mat4 &view = IDENTITY_MATRIX,
                      const glm::mat4 &projection = IDENTITY_MATRIX) {

        glEnable(GL_PRIMITIVE_RESTART);

        glUniformMatrix4fv(M_id_, ONE, DONT_TRANSPOSE, glm::value_ptr(model));
        glUniformMatrix4fv(V_id_, ONE, DONT_TRANSPOSE, glm::value_ptr(view));
        glUniformMatrix4fv(P_id_, ONE, DONT_TRANSPOSE, glm::value_ptr(projection));
        glPrimitiveRestartIndex(65536);

        glDrawElements(GL_TRIANGLE_STRIP, num_indices_, GL_UNSIGNED_INT, 0);

        glDisable(GL_PRIMITIVE_RESTART);
    }

};
