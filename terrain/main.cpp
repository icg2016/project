#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "icg_helper.h"
#include "framebuffer.h"

#include "grid/grid.h"
#include "quad/quad.h"
#include "water/water.h"
#include "trackball.h"
#include "cube/cube.h"
#include "axis/axis.h"
#include "objects/plane/plane.h"
#include "objects/agava/agava.h"
#include "objects/rock/rock.h"
#include "objects/rock2/rock2.h"
#include "helpers/bezier.h"

#include "glm/gtx/string_cast.hpp"
#include "glm/gtx/norm.hpp"

#define DEBUG
#define SIZE 16

#define NB_AGAVA 40
#define NB_ROCK 15

using namespace glm;

/*
 * 1: Mode fly-through
 * 2: Mode FPS
 * 3: Mode camera path
 * */
int CAMERA_MODE = 1;
int window_width = 800;
int window_height = 600;

bool pressing = false, playing = false;
int movement_direction = 1;

float height;

mat4 projection_matrix;
mat4 view_matrix, mirror_view_matrix;

GLuint tId;
GLint texture_width, texture_height;

double old_y = 0;

Trackball trackball;
FrameBuffer fbo, refract_buffer, reflect_buffer;
Grid grid;
Quad quad;
Water water;
Cube sky;
Axis XYZAxis;
Plane plane;
float duration = 10.0f;
Bezier bezier(duration, playing, false);
//Agava agava;
//Rock rock;
//Rock2 rock2;

vec2 def_grid_position = vec2(100.0f);
vec2 grid_position = def_grid_position;
vec3 cam_pos = vec3(0, 1, 0);
vec3 cam_look = vec3(0, 1, 1);
vec3 cam_up = vec3(0, 1, 0);
vec2 anchor_mouse;

float light_x = 2.5f, light_y = 2.0f, light_z = 7.5f, time_started = 0.0f, movement_factor = 0.0f;

mat4 PerspectiveProjection(float fovy, float aspect, float near, float far) {
    mat4 projection = mat4(0.0f);
    float top = near * std::tan(fovy/2);
    float bottom = -top;
    float right = top * aspect;
    float left = -right;
    projection[0][0] = 2*near/(right-left);
    projection[0][2] = (right+left)/(right-left);
    projection[1][1] = 2*near/(top-bottom);
    projection[1][2] = (top+bottom)/(top-bottom);
    projection[2][2] = -(far+near)/(far-near);
    projection[2][3] = -2*far*near/(far-near);
    projection[3][2] = -1;
    return transpose(projection);
}

void Init() {
    glClearColor(0.937, 0.937, 0.937, 1.0);
    glEnable(GL_DEPTH_TEST);

    quad.Init();

    tId = fbo.Init(window_width, window_height);
    GLuint refractId = refract_buffer.Init(window_width, window_height, true, true);
    GLuint reflectId = reflect_buffer.Init(window_width, window_height, true, true);

    grid.Init(tId, SIZE);
    water.Init(refractId, reflectId, SIZE);
    plane.Init("plane.obj");
    //agava.Init("agava.obj", tId, SIZE);
    //rock.Init("rock.obj", tId, SIZE);
    //rock2.Init("rock2.obj", tId, SIZE);

#ifdef DEBUG
    //XYZAxis.Init();
#endif
    sky.Init();
    view_matrix = glm::lookAt(cam_pos,
                              cam_look,
                              cam_up);
    mirror_view_matrix = trackball.getViewMatrix(true);
}

void decay_movement() {
    vec3 off_vec = trackball.pos - trackball.look;
    float horizontal_angle = glm::atan(off_vec.z / off_vec.x);

    if(glm::sign(off_vec.x) == 1) {
        horizontal_angle += 3.1415;
    }

    movement_factor -= 0.2f;
    movement_factor = glm::max(movement_factor, 0.0f);

    grid_position.x += glm::sign(movement_direction) * 0.001 * cos(horizontal_angle) * movement_factor;
    grid_position.y += glm::sign(movement_direction) * 0.001 * sin(horizontal_angle) * movement_factor;
}


void Display() {
    glViewport(0, 0, window_width, window_height);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    fbo.Bind();
    glViewport(0, 0, window_width, window_height);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    quad.Draw(grid_position);
    glEnable(GL_CLIP_DISTANCE0);

    reflect_buffer.Bind();
    glViewport(0, 0, window_width, window_height);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    grid.Draw(IDENTITY_MATRIX, mirror_view_matrix, projection_matrix, light_x, light_y, light_z, grid_position, vec4(0, 1, 0, 0.0));
    sky.Draw(glm::scale(mat4(1.0f), vec3(SIZE)), mirror_view_matrix, projection_matrix, vec4(0, 1, 0, 0.0));
    plane.Draw(IDENTITY_MATRIX, mirror_view_matrix, projection_matrix, light_x, light_y, light_z, SIZE);
    reflect_buffer.Unbind();

    refract_buffer.Bind();
    glViewport(0, 0, window_width, window_height);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    grid.Draw(IDENTITY_MATRIX, view_matrix, projection_matrix, light_x, light_y, light_z, grid_position, vec4(0, -1, 0, -0.0));
    refract_buffer.Unbind();

    glDisable(GL_CLIP_DISTANCE0);

    grid.Draw(IDENTITY_MATRIX, view_matrix, projection_matrix, light_x, light_y, light_z, grid_position, vec4(0, 1, 0, 0.0));
    water.Draw(IDENTITY_MATRIX, view_matrix, projection_matrix, light_x, light_y, light_z, cam_pos, grid_position);
#ifdef DEBUG
    XYZAxis.Draw(IDENTITY_MATRIX, view_matrix, projection_matrix);
#endif
    sky.Draw(glm::scale(mat4(1.0f), vec3(SIZE)), view_matrix, projection_matrix);
    plane.Draw(IDENTITY_MATRIX, view_matrix, projection_matrix, light_x, light_y, light_z, SIZE);
    //agava.Draw(IDENTITY_MATRIX, view_matrix, projection_matrix, light_x, light_y, light_z, SIZE, NB_AGAVA);
    //rock.Draw(IDENTITY_MATRIX, view_matrix, projection_matrix, light_x, light_y, light_z, SIZE, NB_ROCK);
    //rock2.Draw(IDENTITY_MATRIX, view_matrix, projection_matrix, light_x, light_y, light_z, SIZE, 5*NB_ROCK);

    if(movement_factor > 0 && !pressing && CAMERA_MODE != 3) {
        decay_movement();
    }

    if(CAMERA_MODE == 3 && playing) {
        float t = (float)glfwGetTime() - time_started;
        cam_pos = bezier.getPosition(t);
        cam_look =  bezier.getLook(t);

        if(playing) {
            grid_position.x = def_grid_position.x + 0.2 * cam_pos.x;
            grid_position.y = def_grid_position.y + 0.2 * cam_pos.z;

            view_matrix = lookAt(
                        vec3(0, cam_pos.y, 0),
                        cam_look,
                        vec3(0,1,0)
                        );

            mirror_view_matrix = lookAt(
                        vec3(0, -cam_pos.y, 0),
                        vec3(cam_look.x, -cam_look.y, cam_look.z),
                        vec3(0,1,0)
                        );
        }
    }
}

vec2 TransformScreenCoords(GLFWwindow* window, int x, int y) {
    int width;
    int height;
    glfwGetWindowSize(window, &width, &height);
    return vec2(2.0f * (float)x / width - 1.0f,
                1.0f - 2.0f * (float)y / height);
}

void MouseButton(GLFWwindow* window, int button, int action, int mod) {
    if(CAMERA_MODE != 3) {
        // begin anchor for moving camera
        if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
            double x_i, y_i;
            glfwGetCursorPos(window, &x_i, &y_i);
            vec2 p = TransformScreenCoords(window, x_i, y_i);
            trackball.BeingDrag(p.x, p.y);
        }
    }
}

void MousePos(GLFWwindow* window, double x, double y) {
    if(CAMERA_MODE != 3) {
        // moving camera
        if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS) {
            vec2 p = TransformScreenCoords(window, x, y);
            view_matrix = trackball.Drag(p[0], p[1]);
            mirror_view_matrix = trackball.getViewMatrix(true);
        }
    }
}

void SetupProjection(GLFWwindow* window, int width, int height) {
    window_width = width;
    window_height = height;
    glViewport(0, 0, window_width, window_height);

    fbo.Cleanup();


    reflect_buffer.Cleanup();
    refract_buffer.Cleanup();
    grid.Cleanup();
    water.Cleanup();

    water.Init( refract_buffer.Init(window_width, window_height, true, true),  reflect_buffer.Init(window_width, window_height, true, true));
    grid.Init(fbo.Init(window_width, window_height), SIZE);

    projection_matrix = PerspectiveProjection(45.0f,
                                              (GLfloat)window_width / window_height,
                                              0.01f, 1000.0f);
}

void ErrorCallback(int error, const char* description) {
    fputs(description, stderr);
}

void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
    if(action == GLFW_RELEASE) {
        pressing = false;
    }

    if(action == GLFW_PRESS && !playing){
        time_started = (float)glfwGetTime();
        pressing = true;
    }

    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, GL_TRUE);
    }
    if (key == GLFW_KEY_W && action != GLFW_PRESS) {
        movement_direction = 1;

        switch (CAMERA_MODE){
        case 1:
        {
            vec3 off_vec = trackball.pos - trackball.look;
            float time_spent = (float)glfwGetTime() - time_started;

            float horizontal_angle = glm::atan(off_vec.z / off_vec.x);
            float vertical_angle = glm::atan(sqrt(pow(off_vec.x, 2.0) + pow(off_vec.z, 2.0)) / off_vec.y);

            if(glm::sign(off_vec.x) == 1) {
                horizontal_angle += 3.1415;
            }
            if(glm::sign(off_vec.y) == -1) {
                vertical_angle += 3.1415;
            }

            movement_factor = glm::min(pow(time_spent, 2.0), 6.0);
            grid_position.x += 0.001 * cos(horizontal_angle) * sin(vertical_angle) * movement_factor;
            grid_position.y += 0.001 * sin(horizontal_angle) * sin(vertical_angle) * movement_factor;

            trackball.up_offset_ += 0.05 * cos(vertical_angle);
            trackball.radius_ = (1.0f + trackball.up_offset_) / cos(trackball.angles_.y);

            view_matrix = trackball.getViewMatrix();
            mirror_view_matrix = trackball.getViewMatrix(true);
            break;
        }
        case 2:
        {
            vec3 off_vec = trackball.pos - trackball.look;
            float time_spent = (float)glfwGetTime() - time_started;

            float horizontal_angle = glm::atan(off_vec.z / off_vec.x);

            if(glm::sign(off_vec.x) == 1) {
                horizontal_angle += 3.1415;
            }

            movement_factor = glm::min(pow(time_spent, 2.0), 6.0);
            grid_position.x += 0.001 * cos(horizontal_angle) * movement_factor;
            grid_position.y += 0.001 * sin(horizontal_angle) * movement_factor;

            fbo.Bind();
            glReadPixels(window_width/2.0f, window_height/2.0f, 1, 1, GL_RED, GL_FLOAT, &height);
            fbo.Unbind();

            trackball.up_offset_ = -1.0f + glm::max(0.0f, height) + 0.4f;
            trackball.radius_ = (1.0f + trackball.up_offset_) / cos(trackball.angles_.y);

            view_matrix = trackball.getViewMatrix();
            mirror_view_matrix = trackball.getViewMatrix(true);
            break;
        }
        case 3:
            bezier.increaseAcceleration(0.05, (float)glfwGetTime()-time_started);
            break;
        default:
            break;
        }
    }
    if (key == GLFW_KEY_S && action != GLFW_RELEASE) {
        movement_direction = -1;

        switch (CAMERA_MODE){
        case 1:
        {
            vec3 off_vec = trackball.pos - trackball.look;
            float time_spent = (float)glfwGetTime() - time_started;

            float horizontal_angle = glm::atan(off_vec.z / off_vec.x);
            float vertical_angle = glm::atan(sqrt(pow(off_vec.x, 2.0) + pow(off_vec.z, 2.0)) / off_vec.y);

            if(glm::sign(off_vec.x) == 1) {
                horizontal_angle += 3.1415;
            }
            if(glm::sign(off_vec.y) == -1) {
                vertical_angle += 3.1415;
            }

            movement_factor = glm::min(pow(time_spent, 2.0), 6.0);
            grid_position.x -= 0.001 * cos(horizontal_angle) * movement_factor;
            grid_position.y -= 0.001 * sin(horizontal_angle) * movement_factor;

            trackball.up_offset_ -= 0.05 * cos(vertical_angle);
            trackball.radius_ = (1.0f + trackball.up_offset_) / cos(trackball.angles_.y);

            view_matrix = trackball.getViewMatrix();
            mirror_view_matrix = trackball.getViewMatrix(true);
            break;
        }
        case 2:
        {
            vec3 off_vec = trackball.pos - trackball.look;
            float time_spent = (float)glfwGetTime() - time_started;

            float horizontal_angle = glm::atan(off_vec.z / off_vec.x);

            if(glm::sign(off_vec.x) == 1) {
                horizontal_angle += 3.1415;
            }

            movement_factor = glm::min(pow(time_spent, 2.0), 6.0);
            grid_position.x -= 0.001 * cos(horizontal_angle) * movement_factor;
            grid_position.y -= 0.001 * sin(horizontal_angle) * movement_factor;

            fbo.Bind();
            glReadPixels(window_width/2.0f, window_height/2.0f, 1, 1, GL_RED, GL_FLOAT, &height);
            fbo.Unbind();

            trackball.up_offset_ = -1.0f + glm::max(0.0f, height) + 0.4f;
            trackball.radius_ = (1.0f + trackball.up_offset_) / cos(trackball.angles_.y);

            view_matrix = trackball.getViewMatrix();
            mirror_view_matrix = trackball.getViewMatrix(true);
            break;
        }
        case 3:
            bezier.decreaseAcceleration(0.05, (float)glfwGetTime()-time_started);
            break;
        default:
            break;
        }
    }
    if (key == GLFW_KEY_A && action != GLFW_RELEASE) {
        switch (CAMERA_MODE){
        case 2:
        case 1:
        {
            float time_spent = (float)glfwGetTime() - time_started;
            float factor = glm::min(pow(time_spent, 2.0), 3.0);

            trackball.BeingDrag(0, 0);
            view_matrix = trackball.Drag(-0.01 * factor, 0);
            mirror_view_matrix = trackball.getViewMatrix(true);
            break;
        }
        case 3:
            break;
        default:
            break;
        }
    }
    if (key == GLFW_KEY_D && action != GLFW_RELEASE) {
        switch (CAMERA_MODE){
        case 2:
        case 1:
        {
            float time_spent = (float)glfwGetTime() - time_started;
            float factor = glm::min(pow(time_spent, 2.0), 3.0);

            trackball.BeingDrag(0, 0);
            view_matrix = trackball.Drag(0.01 * factor, 0);
            mirror_view_matrix = trackball.getViewMatrix(true);
            break;
        }
        case 3:
            break;
        default:
            break;
        }
    }
    if (key == GLFW_KEY_Q && action != GLFW_RELEASE) {
        switch (CAMERA_MODE){
        case 2:
        case 1:
        {
            float time_spent = (float)glfwGetTime() - time_started;
            float factor = glm::min(pow(time_spent, 2.0), 3.0);
            trackball.BeingDrag(0, 0);
            view_matrix = trackball.Drag(0, 0.01 * factor);
            mirror_view_matrix = trackball.getViewMatrix(true);
            break;
        }
        case 3:
            break;
        default:
            break;
        }
    }
    if (key == GLFW_KEY_E && action != GLFW_RELEASE) {
        switch (CAMERA_MODE){
        case 2:
        case 1:
        {
            float time_spent = (float)glfwGetTime() - time_started;
            float factor = glm::min(pow(time_spent, 2.0), 3.0);

            trackball.BeingDrag(0, 0);
            view_matrix = trackball.Drag(0, -0.01 * factor);
            mirror_view_matrix = trackball.getViewMatrix(true);
            break;
        }
        case 3:
            break;
        default:
            break;
        }
    }

    if(key == GLFW_KEY_1 && action == GLFW_PRESS) {
        trackball.up_offset_ = 0.0f;
        trackball.radius_ = (1.0f + trackball.up_offset_) / cos(trackball.angles_.y);

        view_matrix = trackball.getViewMatrix();
        mirror_view_matrix = trackball.getViewMatrix(true);

        CAMERA_MODE = 1;
    }
    if(key == GLFW_KEY_2 && action == GLFW_PRESS) {
        fbo.Bind();
        glReadPixels(window_width/2.0f, window_height/2.0f, 1, 1, GL_RED, GL_FLOAT, &height);
        fbo.Unbind();

        trackball.up_offset_ = -1.0f + glm::max(0.0f, height) + 0.4f;
        trackball.radius_ = (1.0f + trackball.up_offset_) / cos(trackball.angles_.y);

        view_matrix = trackball.getViewMatrix();
        mirror_view_matrix = trackball.getViewMatrix(true);

        CAMERA_MODE = 2;
    }
    if(key == GLFW_KEY_3 && action == GLFW_PRESS) {
        grid_position = def_grid_position;

        view_matrix = lookAt(CAM_POSITIONS[0], vec3(0,0,0), vec3(0,1,0));
        CAMERA_MODE = 3;
        playing = !playing;
    }
}

int main(int argc, char *argv[]) {
    // GLFW Initialization
    if(!glfwInit()) {
        fprintf(stderr, "Failed to initialize GLFW\n");
        return EXIT_FAILURE;
    }

    glfwSetErrorCallback(ErrorCallback);

    // hint GLFW that we would like an OpenGL 3 context (at least)
    // http://www.glfw.org/faq.html#how-do-i-create-an-opengl-30-context
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // attempt to open the window: fails if required version unavailable
    // note some Intel GPUs do not support OpenGL 3.2
    // note update the driver of your graphic card
    GLFWwindow* window = glfwCreateWindow(window_width, window_height,
                                          "Hello world! :)", NULL, NULL);
    if(!window) {
        glfwTerminate();
        return EXIT_FAILURE;
    }

    // makes the OpenGL context of window current on the calling thread
    glfwMakeContextCurrent(window);

    // set the callback for escape key
    glfwSetKeyCallback(window, KeyCallback);

    // set the framebuffer resize callback
    glfwSetFramebufferSizeCallback(window, SetupProjection);

    // set the mouse press and position callback
    glfwSetMouseButtonCallback(window, MouseButton);
    glfwSetCursorPosCallback(window, MousePos);

    // GLEW Initialization (must have a context)
    // https://www.opengl.org/wiki/OpenGL_Loading_Library
    glewExperimental = GL_TRUE; // fixes glew error (see above link)
    if(glewInit() != GLEW_NO_ERROR) {
        fprintf( stderr, "Failed to initialize GLEW\n");
        return EXIT_FAILURE;
    }

    cout << "OpenGL" << glGetString(GL_VERSION) << endl;

    // initialize our OpenGL program
    Init();
    glfwGetFramebufferSize(window, &window_width, &window_height);
    SetupProjection(window, window_width, window_height);

    double lastTime = glfwGetTime(); int nbFrames = 0;
    while(!glfwWindowShouldClose(window)){
        Display();
        //Compute the time needed to render a frame (average per second)
        double currentTime = glfwGetTime();
        nbFrames++;
        if ( currentTime - lastTime >= 1.0 ){ // If last prinf() was more than 1 sec ago
            printf("%f ms/frame\n", 1000.0/double(nbFrames));
            nbFrames = 0;
            lastTime += 1.0;
        }
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    grid.Cleanup();
    quad.Cleanup();
    water.Cleanup();
    fbo.Cleanup();
#ifdef DEBUG
    XYZAxis.Cleanup();
#endif
    sky.Cleanup();

    glfwDestroyWindow(window);
    glfwTerminate();
    return EXIT_SUCCESS;
}
