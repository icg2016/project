#pragma once
#include "icg_helper.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "../../helpers/texture_helper.h"

class Rock : public Material, public Light {

private:
    GLuint vertex_array_id_;        // vertex array object
    // tinyobjloader shapes (contains points, indices, etc)
    std::vector<tinyobj::shape_t> shapes_;

    GLuint vertex_buffer_object_;           // memory buffer
    GLuint vertex_normal_buffer_object_;    // memory buffer
    GLuint vertex_texcoords_buffer_object_;
    GLuint program_id_;
    GLuint M_id_;                         // model matrix ID
    GLuint V_id_;
    GLuint P_id_;
    GLuint rock_id_;
    GLuint height_map_;
    GLuint grid_dim_id_;
    GLuint position_id_;

    void BindShader(GLuint program_id) {
        glUseProgram(program_id);
        glBindVertexArray(vertex_array_id_);

        // vertex attribute id for positions
        GLint vertex_point_id = glGetAttribLocation(program_id, "vpoint");
        if (vertex_point_id >= 0) {
            glEnableVertexAttribArray(vertex_point_id);

            glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_object_);
            glVertexAttribPointer(vertex_point_id, 3 /*vec3*/, GL_FLOAT,
                                  DONT_NORMALIZE, ZERO_STRIDE, ZERO_BUFFER_OFFSET);
        }

        // vertex attribute id for normals
        GLint vertex_normal_id = glGetAttribLocation(program_id, "vnormal");
        if (vertex_normal_id >= 0) {
            glEnableVertexAttribArray(vertex_normal_id);
            glBindBuffer(GL_ARRAY_BUFFER, vertex_normal_buffer_object_);
            glVertexAttribPointer(vertex_normal_id, 3 /*vec3*/, GL_FLOAT,
                                  DONT_NORMALIZE, ZERO_STRIDE, ZERO_BUFFER_OFFSET);
        }

        GLint texcoords_id = glGetAttribLocation(program_id, "texcoords");
        if (texcoords_id >= 0) {
            glEnableVertexAttribArray(texcoords_id);
            glBindBuffer(GL_ARRAY_BUFFER, vertex_texcoords_buffer_object_);
            glVertexAttribPointer(texcoords_id, 2 /*vec2*/, GL_FLOAT,
                                  DONT_NORMALIZE, ZERO_STRIDE, ZERO_BUFFER_OFFSET);
        }
    }

    void UnbindShader(GLuint program_id) {
        GLint vertex_point_id = glGetAttribLocation(program_id, "vpoint");
        if (vertex_point_id >= 0) {
            glDisableVertexAttribArray(vertex_point_id);
        }

        GLint vertex_normal_id = glGetAttribLocation(program_id, "vnormal");
        if (vertex_normal_id >= 0) {
            glDisableVertexAttribArray(vertex_normal_id);
        }

        GLint texcoords_id = glGetAttribLocation(program_id, "texcoords");
        if (texcoords_id >= 0) {
            glDisableVertexAttribArray(texcoords_id);
        }

        glUseProgram(0);
        glBindVertexArray(0);
    }

public:
    void Init(const string& filename, GLuint height_map,float size = 16.0f) {
        program_id_ = icg_helper::LoadShaders("rock_vshader.glsl",
                                              "rock_fshader.glsl");
        glUseProgram(program_id_);
        glBindVertexArray(vertex_array_id_);

        string error;
        // obj files can contains material informations
        vector<tinyobj::material_t> materials;

        bool objLoadReturn = tinyobj::LoadObj(shapes_, materials, error, filename.c_str());

        if(!error.empty()) {
            cerr << error << endl;
        }

        if(!objLoadReturn) {
            exit(EXIT_FAILURE);
        }

        height_map_ = height_map;
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, height_map_);
        glUniform1i(glGetUniformLocation(program_id_, "heightmap"), 0);
        TextureHelper::load_2Dtexture("texture_rock.bmp", &rock_id_, 1, program_id_, "tex_rock");


        // only load the first shape from the obj file
        // (see tinyobjloader for multiple shapes inside one .obj file)

        int number_of_vertices = shapes_[0].mesh.positions.size();
        int number_of_indices = shapes_[0].mesh.indices.size();
        int number_of_normals = shapes_[0].mesh.normals.size();
        int number_of_texcoords = shapes_[0].mesh.texcoords.size();

        printf("Loaded mesh '%s' (#V=%d, #I=%d, #N=%d, #M=%d)\n", filename.c_str(),
               number_of_vertices, number_of_indices, number_of_normals, number_of_texcoords);

        // vertex one vertex Array
        glGenVertexArrays(1, &vertex_array_id_);
        glBindVertexArray(vertex_array_id_);

        // vertex buffer
        glGenBuffers(ONE, &vertex_buffer_object_);
        glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_object_);
        glBufferData(GL_ARRAY_BUFFER, number_of_vertices * sizeof(float),
                     &shapes_[0].mesh.positions[0], GL_STATIC_DRAW);

        // normal buffer
        glGenBuffers(ONE, &vertex_normal_buffer_object_);
        glBindBuffer(GL_ARRAY_BUFFER, vertex_normal_buffer_object_);
        glBufferData(GL_ARRAY_BUFFER, number_of_normals * sizeof(float),
                     &shapes_[0].mesh.normals[0], GL_STATIC_DRAW);

        // index buffer
        GLuint vertex_buffer_object_indices;
        glGenBuffers(ONE, &vertex_buffer_object_indices);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vertex_buffer_object_indices);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                     number_of_indices * sizeof(unsigned int),
                     &shapes_[0].mesh.indices[0], GL_STATIC_DRAW);

        // texcoords buffer
        glGenBuffers(ONE, &vertex_texcoords_buffer_object_);
        glBindBuffer(GL_ARRAY_BUFFER, vertex_texcoords_buffer_object_);
        glBufferData(GL_ARRAY_BUFFER, number_of_texcoords * sizeof(float),
                     &shapes_[0].mesh.texcoords[0], GL_STATIC_DRAW);

        // unbind
        glBindVertexArray(0);

        V_id_ = glGetUniformLocation(program_id_, "V");
        M_id_ = glGetUniformLocation(program_id_, "M");
        P_id_ = glGetUniformLocation(program_id_, "P");
        grid_dim_id_ = glGetUniformLocation(program_id_, "grid_dim");
        position_id_ = glGetUniformLocation(program_id_, "position");
    }

    void Cleanup() {
        glBindVertexArray(0);
        glUseProgram(0);
        glDeleteBuffers(1, &vertex_buffer_object_);
        glDeleteBuffers(1, &vertex_normal_buffer_object_);
        glDeleteVertexArrays(1, &vertex_array_id_);
    }

    void Draw(const glm::mat4 &model = IDENTITY_MATRIX,
              const glm::mat4 &view = IDENTITY_MATRIX,
              const glm::mat4 &projection = IDENTITY_MATRIX,
              float light_x = 0.0f, float light_y = 0.0f, float light_z = 0.0f,
              const float grid_size = 16.0f, const int NBPLANTS = 10) {
        BindShader(program_id_);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        glUniform1f(grid_dim_id_, grid_size);

        Material::Setup(program_id_);
        Light::Setup(program_id_);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, height_map_);

        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, rock_id_);


        for(int z = 0; z < NBPLANTS; z++) {
            for(int x = 0; x < NBPLANTS; x++) {
                srand(z*NBPLANTS + x);

                float off_z = grid_size/2.0f - (grid_size * z)/(float)NBPLANTS + (rand() % 10 - 5) / 10.0f * grid_size/(float)NBPLANTS;
                float off_x = grid_size/2.0f - (grid_size * x)/(float)NBPLANTS + (rand() % 10 - 5) / 10.0f * grid_size/(float)NBPLANTS;
                glUniform2f(position_id_, off_x, off_z);

                mat4 M = translate(model, vec3(off_x, 0.0, off_z));
                M = glm::rotate(M, rand() % 10 / 10.0f, glm::vec3(0.0f, 1.0f, 0.0f));
                float size = rand() % 10 / 150.0;
                M = glm::scale(M, glm::vec3(size));

                glUniformMatrix4fv(M_id_, ONE, DONT_TRANSPOSE, glm::value_ptr(M));
                glUniformMatrix4fv(V_id_, ONE, DONT_TRANSPOSE, glm::value_ptr(view));
                glUniformMatrix4fv(P_id_, ONE, DONT_TRANSPOSE, glm::value_ptr(projection));
                glDrawElements(GL_TRIANGLES, /*#vertices*/ shapes_[0].mesh.indices.size(),
                        GL_UNSIGNED_INT, ZERO_BUFFER_OFFSET);
            }
        }


        glDisable(GL_BLEND);
        UnbindShader(program_id_);
        glBindVertexArray(0);
        glUseProgram(0);
    }
};
