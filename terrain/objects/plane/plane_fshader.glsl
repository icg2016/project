#version 330

in vec2 uv;
in vec3 light_dir, view_dir, normal_nmv;
in float visibility;

out vec4 color;

uniform vec3 La, Ld, Ls;
uniform vec3 ka, kd, ks;
uniform float alpha;
uniform sampler2D tex_plane;

void main() {
    vec3 r = normalize(reflect(-light_dir, normal_nmv));
    // 1) compute ambient term.
    vec3 ambient = ka*La;
    // 2) compute diffuse term.
    vec3 diffuse = kd*max(dot(normal_nmv, light_dir), 0)*Ld;
    // 3) compute specular term.
    vec3 specular = ks*pow(max(dot(r,view_dir),0), alpha)*Ls;

    color = vec4((ambient + diffuse + specular) * texture(tex_plane, uv).rgb, visibility * 10);
}
