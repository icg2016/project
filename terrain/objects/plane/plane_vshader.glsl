#version 330

uniform mat4 M;
uniform mat4 V;
uniform mat4 P;
uniform vec3 light_pos;

in vec3 vpoint;
in vec3 vnormal;
in vec2 texcoords;

out vec2 uv;
out vec3 light_dir, view_dir, normal_nmv;
out float visibility;

const float density = 0.15;
const float gradient = 6;

void main() {
    uv = texcoords;
    gl_Position = P * V * M * vec4(vpoint, 1.0);

    vec4 vpoint_mv = P * V * M * vec4(vpoint, 1.0);
    light_dir = normalize(light_pos - vpoint.xyz);
    view_dir = -normalize(vpoint_mv.xyz);
    vec4 normal_mv = inverse(transpose(V * M)) * vec4(vnormal, 1.0);
    normal_nmv = normalize(normal_mv.xyz);

    vec4 positionRelToCam = V * M * vec4(vpoint, 1.0);
    float distance = length(positionRelToCam.xyz);
    visibility = exp(-pow((distance*density), gradient));
    visibility = clamp(visibility, 0.0, 1.0);
}
