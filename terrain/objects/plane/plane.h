#pragma once
#include "icg_helper.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "../../helpers/texture_helper.h"

class Plane : public Material, public Light {

private:
    GLuint vertex_array_id_;        // vertex array object
    // tinyobjloader shapes (contains points, indices, etc)
    std::vector<tinyobj::shape_t> shapes_;

    GLuint vertex_buffer_object_;           // memory buffer
    GLuint vertex_normal_buffer_object_;    // memory buffer
    GLuint vertex_texcoords_buffer_object_;
    GLuint program_id_;
    GLuint M_id_;                         // model matrix ID
    GLuint V_id_;
    GLuint P_id_;
    GLuint plane_id_;
    float axis_offset = 0.0f;

    void BindShader(GLuint program_id) {
        glUseProgram(program_id);
        glBindVertexArray(vertex_array_id_);

        // vertex attribute id for positions
        GLint vertex_point_id = glGetAttribLocation(program_id, "vpoint");
        if (vertex_point_id >= 0) {
            glEnableVertexAttribArray(vertex_point_id);

            glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_object_);
            glVertexAttribPointer(vertex_point_id, 3 /*vec3*/, GL_FLOAT,
                                  DONT_NORMALIZE, ZERO_STRIDE, ZERO_BUFFER_OFFSET);
        }

        // vertex attribute id for normals
        GLint vertex_normal_id = glGetAttribLocation(program_id, "vnormal");
        if (vertex_normal_id >= 0) {
            glEnableVertexAttribArray(vertex_normal_id);
            glBindBuffer(GL_ARRAY_BUFFER, vertex_normal_buffer_object_);
            glVertexAttribPointer(vertex_normal_id, 3 /*vec3*/, GL_FLOAT,
                                  DONT_NORMALIZE, ZERO_STRIDE, ZERO_BUFFER_OFFSET);
        }

        GLint texcoords_id = glGetAttribLocation(program_id, "texcoords");
        if (texcoords_id >= 0) {
            glEnableVertexAttribArray(texcoords_id);
            glBindBuffer(GL_ARRAY_BUFFER, vertex_texcoords_buffer_object_);
            glVertexAttribPointer(texcoords_id, 2 /*vec2*/, GL_FLOAT,
                                  DONT_NORMALIZE, ZERO_STRIDE, ZERO_BUFFER_OFFSET);
        }
    }

    void UnbindShader(GLuint program_id) {
        GLint vertex_point_id = glGetAttribLocation(program_id, "vpoint");
        if (vertex_point_id >= 0) {
            glDisableVertexAttribArray(vertex_point_id);
        }

        GLint vertex_normal_id = glGetAttribLocation(program_id, "vnormal");
        if (vertex_normal_id >= 0) {
            glDisableVertexAttribArray(vertex_normal_id);
        }

        GLint texcoords_id = glGetAttribLocation(program_id, "texcoords");
        if (texcoords_id >= 0) {
            glDisableVertexAttribArray(texcoords_id);
        }

        glUseProgram(0);
        glBindVertexArray(0);
    }

public:
    void Init(const string& filename) {
        program_id_ = icg_helper::LoadShaders("plane_vshader.glsl",
                                              "plane_fshader.glsl");

        string error;
        // obj files can contains material informations
        vector<tinyobj::material_t> materials;

        bool objLoadReturn = tinyobj::LoadObj(shapes_, materials, error, filename.c_str());

        if(!error.empty()) {
            cerr << error << endl;
        }

        if(!objLoadReturn) {
            exit(EXIT_FAILURE);
        }

        TextureHelper::load_2Dtexture("texture_plane.png", &plane_id_, 0, program_id_, "tex_plane");

        // only load the first shape from the obj file
        // (see tinyobjloader for multiple shapes inside one .obj file)

        int number_of_vertices = shapes_[0].mesh.positions.size();
        int number_of_indices = shapes_[0].mesh.indices.size();
        int number_of_normals = shapes_[0].mesh.normals.size();
        int number_of_texcoords = shapes_[0].mesh.texcoords.size();

        printf("Loaded mesh '%s' (#V=%d, #I=%d, #N=%d, #M=%d)\n", filename.c_str(),
               number_of_vertices, number_of_indices, number_of_normals, number_of_texcoords);

        // vertex one vertex Array
        glGenVertexArrays(1, &vertex_array_id_);
        glBindVertexArray(vertex_array_id_);

        // vertex buffer
        glGenBuffers(ONE, &vertex_buffer_object_);
        glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_object_);
        glBufferData(GL_ARRAY_BUFFER, number_of_vertices * sizeof(float),
                     &shapes_[0].mesh.positions[0], GL_STATIC_DRAW);

        // normal buffer
        glGenBuffers(ONE, &vertex_normal_buffer_object_);
        glBindBuffer(GL_ARRAY_BUFFER, vertex_normal_buffer_object_);
        glBufferData(GL_ARRAY_BUFFER, number_of_normals * sizeof(float),
                     &shapes_[0].mesh.normals[0], GL_STATIC_DRAW);

        // index buffer
        GLuint vertex_buffer_object_indices;
        glGenBuffers(ONE, &vertex_buffer_object_indices);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vertex_buffer_object_indices);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                     number_of_indices * sizeof(unsigned int),
                     &shapes_[0].mesh.indices[0], GL_STATIC_DRAW);

        // texcoords buffer
        glGenBuffers(ONE, &vertex_texcoords_buffer_object_);
        glBindBuffer(GL_ARRAY_BUFFER, vertex_texcoords_buffer_object_);
        glBufferData(GL_ARRAY_BUFFER, number_of_texcoords * sizeof(float),
                     &shapes_[0].mesh.texcoords[0], GL_STATIC_DRAW);

        // unbind
        glBindVertexArray(0);

        V_id_ = glGetUniformLocation(program_id_, "V");
        M_id_ = glGetUniformLocation(program_id_, "M");
        P_id_ = glGetUniformLocation(program_id_, "P");
    }

    void Cleanup() {
        glBindVertexArray(0);
        glUseProgram(0);
        glDeleteBuffers(1, &vertex_buffer_object_);
        glDeleteBuffers(1, &vertex_normal_buffer_object_);
        glDeleteVertexArrays(1, &vertex_array_id_);
    }

    void Draw(const glm::mat4 &model = IDENTITY_MATRIX,
              const glm::mat4 &view = IDENTITY_MATRIX,
              const glm::mat4 &projection = IDENTITY_MATRIX,
              float light_x = 0.0f, float light_y = 0.0f, float light_z = 0.0f,
              const float grid_size = 16.0f) {
        BindShader(program_id_);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        light_pos = glm::vec3(projection * view * model * glm::vec4(light_x, light_y, light_z, 1.0));

        Material::Setup(program_id_);
        Light::Setup(program_id_);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, plane_id_);

        glm::mat4 M = model;

        float base_z = -1.5f;//-grid_size/2.0;
        float base_x = 0;

        float y = grid_size/6.0;
        float z = mod(base_z + (float)glfwGetTime(), 10.0f);
        float x = base_x + axis_offset;

        if(abs(z) > grid_size/2.0 || abs(x) > grid_size/2.0) {
            //axis_offset = (rand() % 10)/10.0 * grid_size - grid_size/2.0;
            axis_offset = 4.0f;
            //cout << axis_offset << endl;
        }

        M = glm::translate(M, glm::vec3(x, y, z));
        M = glm::rotate(M, 3.1415f, glm::vec3(0.0f, 0.0f, 1.0f));
        M = glm::scale(M, glm::vec3(.02f));

        glUniformMatrix4fv(M_id_, ONE, DONT_TRANSPOSE, glm::value_ptr(M));
        glUniformMatrix4fv(V_id_, ONE, DONT_TRANSPOSE, glm::value_ptr(view));
        glUniformMatrix4fv(P_id_, ONE, DONT_TRANSPOSE, glm::value_ptr(projection));
        glDrawElements(GL_TRIANGLES, /*#vertices*/ shapes_[0].mesh.indices.size(),
                GL_UNSIGNED_INT, ZERO_BUFFER_OFFSET);

        UnbindShader(program_id_);
        glDisable(GL_BLEND);
        glBindVertexArray(0);
        glUseProgram(0);
    }
};
