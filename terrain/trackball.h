#pragma once
#include "icg_helper.h"
#include <glm/gtc/matrix_transform.hpp>
#include "glm/gtx/string_cast.hpp"

using namespace glm;

class Trackball {
    vec2 oldAngles_;
    vec2 anchor_pos_;

public:
    float radius_, up_offset_;
    vec2 angles_;
    vec3 pos, look;

    Trackball() : radius_(1.4142135623731f), angles_(vec2(3.1415/2.0, 3.1415/4.0)), up_offset_(0.0f) {}
    void BeingDrag(float x, float y) {
        anchor_pos_ = vec2(x, y);
        oldAngles_ = angles_;
    }

    mat4 Drag(float x, float y, bool isMirror = false) {
        vec2 diff = vec2(x - anchor_pos_.x, y - anchor_pos_.y);

        angles_ = vec2(2.0 * diff.x, 2.0 * diff.y) + oldAngles_;
        angles_.y = glm::min(glm::max(angles_.y, -1 *half_pi<float>() + 0.01f), half_pi<float>()-0.01f);
        return getViewMatrix(isMirror);
    }

    mat4 getViewMatrix(bool isMirror = false) {
        pos = vec3(0, 1.0 + up_offset_, 0);

        if(isMirror) {
            pos.y *= -1;
            angles_.y *= -1;
        }

        look = vec3(
                    radius_* cos(angles_.x) * cos(angles_.y),
                    radius_* sin(angles_.y),
                    radius_* sin(angles_.x) * cos(angles_.y)
                    );

        if(isMirror) {
            angles_.y *= -1;
        }

        return glm::lookAt(pos,
                           look,
                           vec3(0.0f, 1.0f, 0.0f));

    }
};
