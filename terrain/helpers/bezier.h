#include "paths.h"
#include "icg_helper.h"

using namespace glm;

class Bezier {
public:
    Bezier(float duration, bool &playing, bool loop = true): playing_(playing) {
        loop_ = loop;
        duration_ = duration;
        playing_ = playing;
        acceleration_ = .3f;
        offset_ = 0.0f;

        if(sizeof(CAM_POSITIONS)/sizeof(vec3) != sizeof(CAM_LOOKS)/sizeof(vec3)) {
            throw std::logic_error("Different number of position and look points");
        }

        n_ = sizeof(CAM_POSITIONS)/sizeof(vec3);
    }

    vec3 getPosition(float t) {
        if(loop_) {
            return bezier(CAM_POSITIONS, mod((acceleration_*(t - offset_))/duration_, 1.0f));
        } else {
            if((acceleration_*(t - offset_))/duration_ >= 1.0f) {
                stop();
            } else {
                return bezier(CAM_POSITIONS, (acceleration_*(t - offset_))/duration_);
            }
        }
    }

    vec3 getLook(float t) {
        if(loop_) {
            return bezier(CAM_LOOKS, mod((acceleration_*(t - offset_))/duration_, 1.0f));
        } else {
            if((acceleration_*(t - offset_))/duration_ >= 1.0f) {
                stop();
            } else {
                return bezier(CAM_LOOKS, (acceleration_*(t - offset_))/duration_);
            }
        }
    }

    void increaseAcceleration(float acc = 0.1f, float time=0.0f) {
        if(acceleration_ + acc >= 0) {
            offset_ = (acc * time + acceleration_*offset_) / (acc + acceleration_);
            acceleration_ += acc;
        }
    }

    void decreaseAcceleration(float acc = 0.1f, float time=0.0f) {
        if(acceleration_ - acc >= 0) {
            offset_ = (-acc * time + acceleration_*offset_) / (-acc + acceleration_);
            acceleration_ -= acc;
        }
    }

    void stop() {
        playing_ = false;
    }
private:
    float duration_;
    bool loop_;
    int n_;
    bool &playing_;
    float acceleration_;
    float offset_;

    unsigned long binomial(unsigned long n, unsigned long k) {
        unsigned long i;
        unsigned long b;
        if (0 == k || n == k) {
            return 1;
        }
        if (k > n) {
            return 0;
        }
        if (k > (n - k)) {
            k = n - k;
        }
        if (1 == k) {
            return n;
        }
        b = 1;
        for (i = 1; i <= k; ++i) {
            b *= (n - (k - i));
            if (b < 0) return -1; /* Overflow */
            b /= i;
        }
        return b;
    }

    vec3 bezier(vec3 points[], float t) {
        vec3 res = vec3(0);

        for(int i = 0; i < n_; i++) {
            res += binomial(n_-1, i) * (float)glm::pow(1-t, n_-1-i) * (float)glm::pow(t, i) * points[i];
        }

        return res;
    }
};
