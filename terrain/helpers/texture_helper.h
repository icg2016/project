#pragma once
#include "icg_helper.h"
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/matrix_transform.hpp>

class TextureHelper {

public:
    static void load_2Dtexture(const char* filename, GLuint* tex_id, unsigned int number, GLuint program_id, const char* name) {
        int width, height, nb_component;
        // set stb_image to have the same coordinates as OpenGL
        stbi_set_flip_vertically_on_load(1);
        unsigned char* image = stbi_load(filename, &width,
                                              &height, &nb_component, 0);

        if(image == nullptr) {
            throw(string("Failed to load texture"));
        }

        glGenTextures(1, tex_id);
        glActiveTexture(GL_TEXTURE0 + number);
        glBindTexture(GL_TEXTURE_2D, *tex_id);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

        if(nb_component == 3) {
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0,
                         GL_RGB, GL_UNSIGNED_BYTE, image);
        } else if(nb_component == 4) {
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0,
                         GL_RGBA, GL_UNSIGNED_BYTE, image);
        }

        GLuint water_tex_id = glGetUniformLocation(program_id, name);
        glUniform1i(water_tex_id, number);

        stbi_set_flip_vertically_on_load(0);
        glBindTexture(GL_TEXTURE_2D, 0);
        stbi_image_free(image);
    }

};
