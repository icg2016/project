#include "icg_helper.h"

using namespace glm;

vec3 CAM_POSITIONS[] = {
    vec3(0,1,0),
    vec3(0,1,1),
    vec3(0,1,2),
    vec3(0,1,3),
    vec3(0,1.5,4),
    vec3(0,1.7,5),
    vec3(0,2,5.5),
    vec3(0,1.7,5.75),
    vec3(0,1.5,6),
    vec3(0,1,6),
    vec3(0,1,6),
    vec3(-0.1,1,6.05),
    vec3(-0.15,0.7,6.10),
    vec3(-0.18,1,6.12),
    vec3(-0.20,1,6.15), // virage
    vec3(-0.30,1,6.3),
    vec3(-0.6,1,6),
    vec3(-0.8,1,5.5),
    vec3(-0.6,1,5.2),
    vec3(-0.2,1,5.0), // éviter coin moche
    vec3(-0.2,1,4.7),
    vec3(-0.2,1,4.5),
    vec3(-0.2,1,4.3), // aller tout droit
    vec3(-0.2,1,3.7),
    vec3(-0.2,1,3.5),
    vec3(-0.2,1,3.2),
    vec3(-0.2,1,3),
    vec3(-0.2,1,2.8), // aller/regarder les montagnes
    vec3(-0.3,1,2.5),
    vec3(-0.35,1.2,2.2),
    vec3(-0.45,1.3,2.1),
    vec3(-0.5,1.4,1.9),
    vec3(-0.55,1.4,1.5),
    vec3(-0.8,1.35,1.1),
    vec3(-1.3,1.30,0.7),
    vec3(-1.5,1.35,0.5),
    vec3(-1.6,1.40,0.2),
    vec3(-1.6,1.35,0.0),
    vec3(-1.6,1.3,-0.5)
};

vec3 CAM_LOOKS[] = {
    vec3(0,1,1),
    vec3(-0.4,1,-0.5),
    vec3(0.2,1,1),
    vec3(1,0.5,0.5),
    vec3(1,2,0.5),
    vec3(1,1.7,1),
    vec3(0.75,1.3,1),
    vec3(0.5,2.1,0.9),
    vec3(-0.1,1,0.6),
    vec3(-0.2,0.6,0.5),
    vec3(-0.3,0.5,0.4),
    vec3(-0.3,0.3,0.4),
    vec3(-0.3,0.6,0.4),
    vec3(-0.3,0.8,0.4),
    vec3(-0.3,0.9,0.4), // virage
    vec3(-0.3,1,0),
    vec3(-0.3,1,-0.2),
    vec3(-0.3,1,-0.5),
    vec3(-0.2,1,-0.5),
    vec3(0.2,1,-0.7), // éviter coin moche
    vec3(0.2,1,-1.1),
    vec3(0.2,1,-1.5),
    vec3(0.2,0.9,-1.5), // aller tout droit
    vec3(0.2,0.7,-1.5),
    vec3(0.2,0.6,-1.5),
    vec3(-0.1,0.7,-1.5),
    vec3(-0.3,0.9,-1.5),
    vec3(-0.2,0.9,-1.5), // aller/regarder les montagnes
    vec3(-0.2,0.9,-1.5),
    vec3(-0.2,0.9,-1.5),
    vec3(-0.2,0.9,-1.5),
    vec3(-0.3,1,-1.6),
    vec3(-0.4,1.1,-1.7),
    vec3(-1,1,-1.8),
    vec3(-0.8,0.9,-1.8),
    vec3(-0.6,0.9,-1.8),
    vec3(-0.4,0.9,-2),
    vec3(-0.2,0.9,-2.2),
    vec3(0.2,0.9,-2.5)
};
