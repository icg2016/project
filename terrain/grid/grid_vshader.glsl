#version 330

uniform mat4 M;
uniform mat4 V;
uniform mat4 P;
uniform vec3 light_pos;
uniform vec4 plane;
uniform sampler2D heightmap;
uniform float gridDim;

in vec2 position;
in vec3 vpoint;

out vec2 uv;
out vec3 light_dir, view_dir;
out float visibility, height;

const float density = 0.15;
const float gradient = 6;

void main() {
    uv = (position + (gridDim/2.0f)) / gridDim;
    height = texture(heightmap, uv).r;

    vec4 world_position = M * vec4(position.x, height + 0.2f,  position.y, 1.0f);
    gl_ClipDistance[0] = dot(world_position, plane);
    gl_Position = P * V * world_position;

    vec4 vpoint_mv = P * V * M * vec4(vpoint, 1.0);
    light_dir = normalize(light_pos - vpoint.xyz);
    view_dir = -normalize(vpoint_mv.xyz);


    vec4 positionRelToCam = V * vec4(position.x, height,  position.y, 1.0f);
    float distance = length(positionRelToCam.xyz);
    visibility = exp(-pow((distance*density), gradient));
    visibility = clamp(visibility, 0.0, 1.0);
}
