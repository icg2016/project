#version 330

out vec3 color;

in vec2 uv;
in vec3 light_dir, view_dir;

uniform sampler2D heightmap;
uniform sampler2D tex_snow;
uniform sampler2D tex_grass;
uniform sampler2D tex_rock;
uniform sampler2D tex_sand;
uniform sampler2D tex_seabed;
uniform vec2 location;

uniform vec3 La, Ld, Ls;
uniform vec3 ka, kd, ks;
uniform float alpha;

in vec3 n;
in float height, visibility;

float pow4 (float x) {
    return x*x*x*x;
}

float pow2 (float x) {
    return x*x;
}

void main() {
    float height_offset = 0.2f;

    float a_snow = exp(-pow4((height-1.2)*3));
    float a_rock = exp(-pow4((height-0.7)*4));
    float a_grass = exp(-pow4((height-0.2)*4));
    float a_sand = exp(-pow2((height+0.2)*8));
    float a_seabed = exp(-pow2((height+0.5)*5));

    float h1 = texture(heightmap, vec2(uv.x + 0.01, uv.y)).r;
    float h3 = texture(heightmap, vec2(uv.x - 0.01, uv.y)).r;
    vec3 uv1 = vec3(uv.x + 0.01, h1, uv.y);
    vec3 uv3 = vec3(uv.x - 0.01, h3, uv.y);

             // Y delta
    float h2 = texture(heightmap, vec2(uv.x, uv.y+0.01)).r;
    float h4 = texture(heightmap, vec2(uv.x, uv.y - 0.01)).r;
    vec3 uv2 = vec3(uv.x, h2, uv.y + 0.01);
    vec3 uv4 = vec3(uv.x, h4, uv.y - 0.01);

    vec3 n = /*normal_compute(location);//*/normalize(cross(uv1 - uv3 , uv2 - uv4));
    float c = clamp(dot(n, light_dir), 0, 1);

    vec3 base = texture(tex_sand, 10*(uv + location)).rgb * a_sand +
            texture(tex_snow, 10*(uv + location)).rgb * a_snow +
            texture(tex_grass, 10*(uv + location)).rgb * a_grass+
            texture(tex_rock, 10*(uv + location)).rgb * a_rock +
            texture(tex_seabed, 10*(uv + location)).rgb * a_seabed;



    vec3 r = normalize(reflect(-light_dir, n));
    // 1) compute ambient term.
    vec3 ambient = ka*La;
    // 2) compute diffuse term.
    vec3 diffuse = kd*max(dot(n, light_dir), 0)*Ld;
    // 3) compute specular term.
    vec3 specular = ks*pow(max(dot(r,view_dir),0), alpha)*Ls;

    //color = vec4((ambient + diffuse + specular) * base, visibility);
    color = mix(vec3(222.0/255.0, 231.0/255.0, 225.0f/255.0), (ambient + diffuse + specular) * base, visibility);
}
