#pragma once
#include "icg_helper.h"
#include "../geo/SimpleGrid.h"
#include "../helpers/texture_helper.h"
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <glm/fwd.hpp>

glm::vec3 light_pos = glm::vec3(0.0f, 1.0f, 1.0f);

struct Light {
    glm::vec3 La = glm::vec3(1.0f, 1.0f, 1.0f);
    glm::vec3 Ld = glm::vec3(1.0f, 1.0f, 1.0f);
    glm::vec3 Ls = glm::vec3(1.0f, 1.0f, 1.0f);

    // pass light properties to the shader
    void Setup(GLuint program_id) {
        glUseProgram(program_id);

        // given in camera space
        GLuint light_pos_id = glGetUniformLocation(program_id, "light_pos");

        GLuint La_id = glGetUniformLocation(program_id, "La");
        GLuint Ld_id = glGetUniformLocation(program_id, "Ld");
        GLuint Ls_id = glGetUniformLocation(program_id, "Ls");

        glUniform3fv(light_pos_id, ONE, glm::value_ptr(light_pos));
        glUniform3fv(La_id, ONE, glm::value_ptr(La));
        glUniform3fv(Ld_id, ONE, glm::value_ptr(Ld));
        glUniform3fv(Ls_id, ONE, glm::value_ptr(Ls));
    }
};

struct Material {
    glm::vec3 ka = glm::vec3(0.4f, 0.4f, 0.4f);
    glm::vec3 kd = glm::vec3(0.5f, 0.5f, 0.5f);
    glm::vec3 ks = glm::vec3(0.1f, 0.1f, 0.1f);
    float alpha = 50.0f;

    // pass material properties to the shaders
    void Setup(GLuint program_id) {
        glUseProgram(program_id);

        GLuint ka_id = glGetUniformLocation(program_id, "ka");
        GLuint kd_id = glGetUniformLocation(program_id, "kd");
        GLuint ks_id = glGetUniformLocation(program_id, "ks");
        GLuint alpha_id = glGetUniformLocation(program_id, "alpha");

        glUniform3fv(ka_id, ONE, glm::value_ptr(ka));
        glUniform3fv(kd_id, ONE, glm::value_ptr(kd));
        glUniform3fv(ks_id, ONE, glm::value_ptr(ks));
        glUniform1f(alpha_id, alpha);
    }
};

class Grid : public Material, public Light, public SimpleGrid {

private:
    GLuint height_map;
    GLuint snow_id_, rock_id_, grass_id_, sand_id_, seabed_id_, position_id_, clip_plane_id_;

public:
    void Init(GLuint texId, float size = 16.0f, glm::vec4 clip_plane = glm::vec4(0)) {
        SimpleGrid::Init("grid", size, 512);

        glUseProgram(program_id_);
        glBindVertexArray(vertex_array_id_);

        height_map = texId;
        {
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, height_map);
            glUniform1i(glGetUniformLocation(program_id_, "heightmap"), 0);

            TextureHelper::load_2Dtexture("texture_snow.tga", &snow_id_, 1, program_id_, "tex_snow");
            TextureHelper::load_2Dtexture("texture_rock.tga", &rock_id_, 2, program_id_, "tex_rock");
            TextureHelper::load_2Dtexture("texture_sand.tga", &sand_id_, 3, program_id_, "tex_sand");
            TextureHelper::load_2Dtexture("texture_grass.tga", &grass_id_, 4, program_id_, "tex_grass");
            TextureHelper::load_2Dtexture("texture_seabed.tga", &seabed_id_, 5, program_id_, "tex_seabed");
        }

        clip_plane_id_ = glGetUniformLocation(program_id_, "plane");
        position_id_ = glGetUniformLocation(program_id_, "location");

        V_id_ = glGetUniformLocation(program_id_, "V");
        M_id_ = glGetUniformLocation(program_id_, "M");
        P_id_ = glGetUniformLocation(program_id_, "P");

        glBindTexture(GL_TEXTURE_2D, 0);
        glBindTexture(GL_TEXTURE_1D, 0);
        glBindVertexArray(0);
        glUseProgram(0);
    }

    void Cleanup() {
        glBindVertexArray(0);
        glUseProgram(0);
        glDeleteTextures(1, &height_map);
        glDeleteTextures(1, &grass_id_);
        glDeleteTextures(1, &rock_id_);
        glDeleteTextures(1, &snow_id_);
        glDeleteTextures(1, &sand_id_);
        glDeleteTextures(1, &seabed_id_);
        SimpleGrid::Cleanup();
    }

    void Draw(const glm::mat4 &model = IDENTITY_MATRIX,
              const glm::mat4 &view = IDENTITY_MATRIX,
              const glm::mat4 &projection = IDENTITY_MATRIX,
              float light_x = 0.0f, float light_y = 0.0f, float light_z = 0.0f,
              glm::vec2 position = glm::vec2(0), glm::vec4 clip_plane = glm::vec4(0)) {
        glUseProgram(program_id_);
        glBindVertexArray(vertex_array_id_);

        //glEnable(GL_BLEND);
        //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        light_pos = glm::vec3(projection * view * model * glm::vec4(light_x, light_y, light_z, 1.0));

        Material::Setup(program_id_);
        Light::Setup(program_id_);

        glUniform2f(position_id_, position.x, position.y);

        glUniform4f(clip_plane_id_, clip_plane.x, clip_plane.y, clip_plane.z, clip_plane.w);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, height_map);

        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, snow_id_);

        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, rock_id_);

        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, sand_id_);

        glActiveTexture(GL_TEXTURE4);
        glBindTexture(GL_TEXTURE_2D, grass_id_);

        glActiveTexture(GL_TEXTURE5);
        glBindTexture(GL_TEXTURE_2D, seabed_id_);

        SimpleGrid::Draw(model, view, projection);

        //glDisable(GL_BLEND);

        glBindVertexArray(0);
        glUseProgram(0);
    }
};
