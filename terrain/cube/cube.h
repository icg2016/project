#pragma once
#include "icg_helper.h"
#include "glm/gtc/type_ptr.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include <glm/gtx/rotate_vector.hpp>

static const glm::vec3 CubeVertices[] =
        {
                glm::vec3(-0.5, -0.5, -0.5),
                glm::vec3(-0.5, 0.5, -0.5),
                glm::vec3(0.5, -0.5, -0.5),
                glm::vec3(-0.5, 0.5, -0.5),
                glm::vec3(0.5, -0.5, -0.5),
                glm::vec3(0.5, 0.5, -0.5),
                glm::vec3(0.5, 0.5, 0.5),
                glm::vec3(0.5, -0.5, 0.5),
                glm::vec3(0.5, 0.5, -0.5),
                glm::vec3(0.5, -0.5, 0.5),
                glm::vec3(0.5, 0.5, -0.5),
                glm::vec3(0.5, -0.5, -0.5),
                glm::vec3(0.5, 0.5, 0.5),
                glm::vec3(-0.5, 0.5, 0.5),
                glm::vec3(0.5, -0.5, 0.5),
                glm::vec3(-0.5, 0.5, 0.5),
                glm::vec3(0.5, -0.5, 0.5),
                glm::vec3(-0.5, -0.5, 0.5),
                glm::vec3(-0.5, -0.5, 0.5),
                glm::vec3(-0.5, -0.5, -0.5),
                glm::vec3(0.5, -0.5, 0.5),
                glm::vec3(-0.5, -0.5, -0.5),
                glm::vec3(0.5, -0.5, 0.5),
                glm::vec3(0.5, -0.5, -0.5),
                glm::vec3(-0.5, 0.5, -0.5),
                glm::vec3(-0.5, -0.5, -0.5),
                glm::vec3(-0.5, 0.5, 0.5),
                glm::vec3(-0.5, -0.5, -0.5),
                glm::vec3(-0.5, 0.5, 0.5),
                glm::vec3(-0.5, -0.5, 0.5),
                glm::vec3(0.5, 0.5, -0.5),
                glm::vec3(-0.5, 0.5, -0.5),
                glm::vec3(0.5, 0.5, 0.5),
                glm::vec3(-0.5, 0.5, -0.5),
                glm::vec3(0.5, 0.5, 0.5),
                glm::vec3(-0.5, 0.5, 0.5)
        };


class Cube {

    private:
        GLuint program_id_;             // GLSL shader program ID
        GLuint texture_id_;             // texture ID
        GLuint skyboxVAO, skyboxVBO;

        void loadCubemap() {
            glGenTextures(1, &texture_id_);
            glActiveTexture(GL_TEXTURE0);

            int width, height, nb_component;
            unsigned char* image;

            vector<const GLchar*> faces;
            faces.push_back("right.png");
            faces.push_back("left.png");
            faces.push_back("top.png");
            faces.push_back("bottom.png");
            faces.push_back("back.png");
            faces.push_back("front.png");


            glBindTexture(GL_TEXTURE_CUBE_MAP, texture_id_);
            stbi_set_flip_vertically_on_load(0);
            for(GLuint i = 0; i < faces.size(); i++)
            {
                image = stbi_load(faces[i], &width, &height, &nb_component, 0);
                glTexImage2D(
                        GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0,
                        GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image
                );
                stbi_image_free(image);
            }
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

            glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
        }


    public:
        void Init() {
            program_id_ = icg_helper::LoadShaders("cube_vshader.glsl",
                                                  "cube_fshader.glsl");
            if(!program_id_) {
                exit(EXIT_FAILURE);
            }

            glUseProgram(program_id_);

            glGenVertexArrays(1, &skyboxVAO);
            glBindVertexArray(skyboxVAO);

            glGenBuffers(1, &skyboxVBO);
            glBindBuffer(GL_ARRAY_BUFFER, skyboxVBO);

            glBufferData(GL_ARRAY_BUFFER, sizeof(CubeVertices), &CubeVertices, GL_STATIC_DRAW);
            GLint posAttrib = glGetAttribLocation(program_id_, "position");
            glEnableVertexAttribArray(posAttrib);
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);

            glEnableVertexAttribArray(0);
            glBindVertexArray(0);

            loadCubemap();

            glUseProgram(0);
        }

        void Cleanup() {
            glBindVertexArray(0);
            glUseProgram(0);
            glDeleteBuffers(1, &skyboxVBO);
            glDeleteVertexArrays(1, &skyboxVAO);
            glDeleteProgram(program_id_);
            glDeleteTextures(1, &texture_id_);
        }

        void Draw(const glm::mat4& model = IDENTITY_MATRIX,
                  const glm::mat4& view = IDENTITY_MATRIX,
                  const glm::mat4& projection = IDENTITY_MATRIX, glm::vec4 clip_plane = glm::vec4(0)){
            glUseProgram(program_id_);
            glBindVertexArray(skyboxVAO);

            //glm::mat4 MVP = projection * view * model;
            glUniformMatrix4fv(glGetUniformLocation(program_id_, "M"), 1, GL_FALSE, value_ptr(model));
            glUniformMatrix4fv(glGetUniformLocation(program_id_, "V"), 1, GL_FALSE, value_ptr(view));
            glUniformMatrix4fv(glGetUniformLocation(program_id_, "P"), 1, GL_FALSE, value_ptr(projection));

            glUniform4f(glGetUniformLocation(program_id_, "plane"), clip_plane.x, clip_plane.y, clip_plane.z, clip_plane.w);

            glActiveTexture(GL_TEXTURE0);
            glUniform1i(glGetUniformLocation(program_id_, "skybox"), 0);
            glBindTexture(GL_TEXTURE_CUBE_MAP, texture_id_);
            glDrawArrays(GL_TRIANGLES, 0, 36);
            glBindVertexArray(0);

            glUseProgram(0);
        }
};
