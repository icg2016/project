#version 330 core

uniform mat4 M;
uniform mat4 V;
uniform mat4 P;
uniform vec4 plane;

in vec3 position;
out vec3 TexCoords;


void main(){
    vec4 world_position = M * vec4(position.x, position.y, position.z, 1.0f);
    gl_ClipDistance[0] = dot(world_position, plane);
    gl_Position = P * V * world_position;

    TexCoords = position;
}
