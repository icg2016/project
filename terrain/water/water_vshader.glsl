#version 330 core
const float pi = 3.14159f;

uniform mat4 M;
uniform mat4 V;
uniform mat4 P;
uniform float time;
uniform float gridDim;
uniform vec3 light_pos;
uniform vec3 cam_position;
uniform vec3 cam_look;

in vec2 position;

out float visibility, refract_factor;
out vec4 to_camera_vector;
out vec2 uv;
out vec4 from_light_vector;
out vec3 light_dir;

const float density_ref = 0.4;
const float gradient_ref = 1;

const float density = 0.15;
const float gradient = 6;

void main() {
    /*uv = (position + (gridDim/2.0f)) / gridDim;

    float amp = pi*30;
    float sinp = 0.02*(sin(amp*(uv.x + uv.y) - pi*time) - 1);
    float cosp = 0.01*(cos(amp/2*(-uv.x + uv.y) + pi/4*time) - 1);

    float height = (sinp + cosp)/2 - 0.2;
    vec3 pos_3d = vec3(position.x, height, -position.y);

    gl_Position = P * V * M * vec4(pos_3d, 1.0);*/

    vec3 pos_3d = vec3(position.x, 0.0, position.y);
    vec4 positionRelToCam = V * vec4(pos_3d, 1.0);
    float distance = length(positionRelToCam.xyz);

    refract_factor = exp(-pow((distance*density_ref), gradient_ref));
    refract_factor = clamp(refract_factor, 0.0, 1.0);

    visibility = exp(-pow((distance*density), gradient));
    visibility = clamp(visibility, 0.0, 1.0);

    vec4 world_position = M * vec4(position.x, 0.0, position.y, 1.0);
    gl_Position = P * V * world_position;
    to_camera_vector = V * vec4(cam_position, 1.0) - V * vec4(world_position.xyz, 1.0);
    uv = vec2(position.x/2.0 + 0.5, position.y/2.0 + 0.5) * 6.0;
    from_light_vector = V * vec4(world_position.xyz, 1.0) - V* vec4(light_pos, 1.0);
}
