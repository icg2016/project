#pragma once
#include "icg_helper.h"
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "../helpers/texture_helper.h"

class Water : public Material, public Light, public SimpleGrid {

private:
    GLuint depth_id_, normal_map_id_, dudv_id_, refract_id_, reflect_id_, program_id_;

public:
    void Init(GLuint refract_id, GLuint reflect_id, float size = 16.0f) {
        SimpleGrid::Init("water", size, 512);
        program_id_ = icg_helper::LoadShaders("water_vshader.glsl",
                                              "water_fshader.glsl");

        glUseProgram(program_id_);
        glBindVertexArray(vertex_array_id_);


        refract_id_ = refract_id;
        reflect_id_ = reflect_id;

        GLuint tex_refract_id = glGetUniformLocation(program_id_, "tex_refract");
        glUniform1i(tex_refract_id, 0 /*GL_TEXTURE0*/);
        GLuint tex_reflect_id = glGetUniformLocation(program_id_, "tex_reflect");
        glUniform1i(tex_reflect_id, 1 /*GL_TEXTURE1*/);
        TextureHelper::load_2Dtexture("dudv_water.png", &dudv_id_, 2, program_id_, "dudv");
        TextureHelper::load_2Dtexture("normal_map_water.png", &normal_map_id_, 3, program_id_, "normal_map");


        //TextureHelper::load_2Dtexture("texture_water.tga", &water_id_, 0, program_id_, "tex_water");
        {
            //glActiveTexture(GL_TEXTURE1);
            //glBindTexture(GL_TEXTURE_2D, refract_id_);
            //glUniform1i(glGetUniformLocation(program_id_, "tex_refract"), 0);
            //glBindTexture(GL_TEXTURE_2D, 1);

            //glActiveTexture(GL_TEXTURE2);
            //glBindTexture(GL_TEXTURE_2D, reflect_id_);
            //glUniform1i(glGetUniformLocation(program_id_, "tex_reflect"), 1);
            //glBindTexture(GL_TEXTURE_2D, 2);
        }


        // to avoid the current object being polluted
        glBindVertexArray(0);
        glUseProgram(0);
    }

    void Cleanup() {
        glBindVertexArray(0);
        glUseProgram(0);
        glDeleteTextures(1, &normal_map_id_);
        glDeleteTextures(1, &dudv_id_);
        glDeleteTextures(1, &refract_id_);
        glDeleteTextures(1, &reflect_id_);
        SimpleGrid::Cleanup();
    }

    void Draw(const glm::mat4 &model = IDENTITY_MATRIX,
              const glm::mat4 &view = IDENTITY_MATRIX,
              const glm::mat4 &projection = IDENTITY_MATRIX,
              float light_x = 0.0f, float light_y = 0.0f, float light_z = 0.0f,
               const glm::vec3 cam_position = glm::vec3(0), glm::vec2 location = glm::vec2(0)) {

        light_pos = glm::vec3(light_x, light_y, light_z);

        glUseProgram(program_id_);
        glBindVertexArray(vertex_array_id_);

        /*glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, water_id_);*/

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, refract_id_);

        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, reflect_id_);

        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, dudv_id_);

        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, normal_map_id_);

        Material::Setup(program_id_);
        Light::Setup(program_id_);

        glUniform2f(glGetUniformLocation(program_id_, "location"), location.x, location.y);
        glUniform1f(glGetUniformLocation(program_id_, "time"), glfwGetTime());
        glUniform3f(glGetUniformLocation(program_id_, "cam_position"), cam_position.x, cam_position.y, cam_position.z);
        glUniform3f(glGetUniformLocation(program_id_, "light_pos"), light_pos.x, light_pos.y, light_pos.z);


        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        SimpleGrid::Draw(model, view, projection);

        glDisable(GL_BLEND);

        glBindVertexArray(0);
        glUseProgram(0);
    }
};
