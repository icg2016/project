#version 330 core

in float visibility, refract_factor;
in vec4 to_camera_vector;
in vec2 uv;
in vec4 from_light_vector;

out vec4 color;

uniform sampler2D tex_refract;
uniform sampler2D tex_reflect;
uniform sampler2D dudv;
uniform sampler2D normal_map;
uniform sampler2D depth;
uniform float time;
uniform vec2 location;


const float waveStrength = 0.01;
const float waveSpeed = 1/16.0;
const float unitSize = 3.0;
const float shineDamper = 20.0;
const float reflectivity = 0.6;
const vec3 light_color = vec3(1.0, 1.0, 1.0);

uniform vec3 La, Ld, Ls;
uniform vec3 ka, kd, ks;
uniform float alpha;

void main() {
    //color = vec4(mix(vec3(222.0/255.0, 231.0/255.0, 225.0f/255.0), texture(tex_water, uv).xyz, visibility), alpha);

    ivec2 dim = textureSize(tex_reflect, 0);
    float dx = dim.x;
    float dy = dim.y;
    float u = gl_FragCoord.x / dx;
    float v = gl_FragCoord.y / dy;

    /*vec2 dist1 = (texture(dudv, vec2(uv.x/4.0 + time/16.0, uv.y/4.0)).rg * 2.0 - 1.0) * 0.01;
    vec2 dist2 = (texture(dudv, vec2(-uv.x/4.0, uv.y/4.0 + time/16.0)).rg * 2.0 - 1.0) * 0.01;
    vec2 dist = dist1 + dist2;*/

    vec2 distTexCoords = texture(dudv, vec2(uv.x /unitSize + time * waveSpeed, uv.y/unitSize)).rg*0.01;
    distTexCoords = uv/unitSize + vec2(distTexCoords.y, distTexCoords.y + time * waveSpeed);
    vec2 totDist = (texture(dudv, distTexCoords).rg * 2.0 - 1.0) * waveStrength;

    vec4 refraction_color = texture(tex_refract, vec2(u,v) + totDist);
    vec4 reflection_color = texture(tex_reflect, vec2(u,1-v) + totDist);

    vec3 view_vector = normalize(to_camera_vector.xyz);
    vec4 normal_map_colour = texture(normal_map, distTexCoords + location * 10.0f);
    vec3 normal = vec3(normal_map_colour.r * 2.0 - 1.0, normal_map_colour.b, normal_map_colour.g * 2.0 - 1.0);
    normal = normalize(normal);

    vec3 r = reflect(normalize(from_light_vector.xyz), normal);
    float specular = max(dot(r,view_vector),0.0);
    specular = pow(specular, shineDamper);
    vec3 specular_highlights = light_color * specular * reflectivity;


    color = mix(reflection_color, refraction_color, refract_factor);
    color = mix(color, vec4(0, 0.3, 0.5, 1.0), 0.2) + vec4(specular_highlights, 0.0);
    color = mix(vec4(247.0/255.0, 249.0/255.0, 245.0f/255.0, 1.0f), color, visibility);
}
